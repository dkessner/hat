Hat project
===========

This project contains Arduino code for the LED hat in addition to a
simulator written in Processing.  A set of high-level functions to
set pixels is implemented in both Arduino and Processing, so that
the loop code is identical (but is compiled as C for Arduino and
Java for Processing).


- led/hat.ino: Arduino code for the hat (150 LEDs, arranged 8x19),
        including high-level functions for setting pixels, but
        excluding the loop() function.

- led/loop.ino: The loop() and auxilliary functions.

- sim_led/loop.pde: Identical to loop.ino.

- sim_led/sim_led.pde: Processing simulation of the Arduino
        loop, including implementation of the same high-level 
        functions in hat.ino.

- serial_test: test code for serial connection between Raspberry Pi and Arduino


