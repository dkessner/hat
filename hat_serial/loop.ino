//
// loop.pde
//
// Darren Kessner
//


// available functions:
//
//  show();
//  set(int i, int c);
//  set2d(int i, int j, int c);
//  setAll(int c);
//  int color(int r, int g, int b); // encodes (r,g,b) as int
//  int rainbow(float t);           // maps t in [0,1] to rainbow color
//


int currentIndex = 0;
typedef void (*LoopFunction)();

LoopFunction loopFunctions[] =
{
    loop_none,
    loop_rgb,
    loop_violet,
    loop_blue_wave,
    loop_bouncing_row,
    loop_random_sparkles,
    loop_chase,
    loop_rainbow_cycle,
    loop_rainbow_cycle_2
};

const int loopFunctionCount = sizeof(loopFunctions)/sizeof(LoopFunction);

unsigned long timeout = 0;


void loop()
{
    if (Serial.available())
    {
        String temp = Serial.readString();
        int index = temp.toInt();

        if (index >= 0 && index < loopFunctionCount)
        {
            currentIndex = index;
            timeout = millis() + 10000;
        }
    }

    if (millis() > timeout)
        currentIndex = 0;

    loopFunctions[currentIndex](); 
}


void loop_none()
{
    setAll(0);
    show();
    delay(2000);
}


void loop_test1()
{
    set(2, color(0, 0, 255));
    show();
    delay(1000);
}


void loop_test2()
{
    set2d(3, 4, color(0, 0, 255));
    show();
    delay(1000);
}


void loop_rgb()
{  
    setAll(color(255, 0, 0));
    show();
    delay(500);

    setAll(color(0, 255, 0));
    show();
    delay(500);

    setAll(color(0, 0, 255));
    show();
    delay(500);

    setAll(0);
    show();
    delay(500);
}


void loop_rainbow_cycle()
{
    setAll(rainbow(0));
    show();
    delay(500);

    for (float x=0; x<=1; x+=.01)
    {
       setAll(rainbow(x)); 
       show();
       delay(10);
    }

    setAll(0);
    delay(500);
}


void loop_violet()
{
    setAll(color(204, 0, 153)); // violet
    show();
    delay(2000);
}


void loop_rainbow_cycle_2()
{
    for (float x=0; x<=1; x+=.01)
    {
        for (int i=0; i<ledCount; i++)
        {
            float t = i/(float)columnCount;
            set(i, rainbow(x+t));
        }
        show();
        delay(10);
    }
    setAll(0);
    show();
}


void loop_chase()
{
    for (int i=0; i<ledCount; i++)
    {
        set(i, color(0, 255, 0));
        show();
        delay(50);
        set(i, 0);
    }
    setAll(0);
    show();
    delay(50);
}


void loop_move_pixels()
{
    int m = color(0, 0, 255);
    int o = color(0, 255, 0);

    int pixels[] = {0, m, m, m, m, m, 0,
                    m, 0, 0, 0, 0, 0, m,
                    m, 0, o, 0, o, 0, m,
                    m, 0, 0, 0, 0, 0, m,
                    m, o, 0, 0, 0, o, m,
                    m, 0, o, o, o, 0, m,
                    m, 0, 0, 0, 0, 0, m,
                    0, m, m, m, m, m, 0,
    };

    int gridWidth = 7;
    int gridHeight = 8;

    int x = 0;
    int vx = 1;

    for (int i=0; i<100; i++)
    {
      setPixels(0, x, pixels, gridHeight, gridWidth);
      show();
      delay(100);

      unsetPixels(0, x, gridHeight, gridWidth);
      show();

      x += vx;

      if (x <= 0 || x >= columnCount - gridWidth)
        vx = -vx;
    }
}


void loop_random_sparkles()
{
    setAll(0);
    show();

    for (int iteration=0; iteration<30; iteration++)
    {
        for (int sparkle=0; sparkle<10; sparkle++)
        {
            int i = (int)random(rowCount);
            int j = (int)random(columnCount);
            set2d(i, j, color(random(256), random(256), random(256)));
        }

        show();
        delay(200);

        for (int i=0; i<rowCount; i++)
        for (int j=0; j<columnCount; j++)
            set2d(i, j, 0);
        show();
    }
}


void loop_blue_wave()
{
    for (int intensity = 0; intensity<256; intensity++)
    {
        setAll(color(0, 0, intensity));
        show();
        delay(5);
    }
}


void loop_bouncing_row()
{
    int i=0; 
    int vi = 1;

    float x = 0;
    float vx = .01;

    for (int iteration=0; iteration<50; iteration++)
    {
      int c = rainbow(x);

      for (int j=0; j<columnCount; j++)
        set2d(i, j, c);

      show();
      delay(100);

      for (int j=0; j<columnCount; j++)
        set2d(i, j, 0);

      show();

      i += vi;
      if (i==0 || i>=rowCount-1)
        vi *= -1;

      x += vx;
    }
}


int ys[] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
int vys[] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
int cs[] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};


void loop_bouncing_columns()
{
    if (cs[0] == 0)
    {
      for (int j=0; j<columnCount; j++)
      {
          ys[j] = (int)random(1, rowCount-1);
          vys[j] = random(1)<.5 ? 1 : -1;
          cs[j] = color(random(256), random(256), random(256));
      }
    }

    int i = 0;
    int vi = 1;

    /*
    for (int iteration=0; iteration<40; iteration++)
    {
        set2d(i, 2, cs[2]);
        show();
        delay(200);
        set2d(i, 2, 0);
        show();
        delay(0);

        i += vi;
        if (i==0 || i>=rowCount-1)
            vi *= -1;
    }
    */


    for (int iteration=0; iteration<40; iteration++)
    {
        for (int j=0; j<columnCount; j++)
            set2d(ys[j], j, cs[j]);
        show();
        delay(200);

        for (int j=0; j<columnCount; j++)
            set2d(ys[j], j, 0);
        //show();

        for (int j=0; j<columnCount; j++)
        {
            ys[j] += vys[j];
            if (ys[j] == 0 || ys[j] >= rowCount-1)
                vys[j] *= -1;
        }

    }
}



