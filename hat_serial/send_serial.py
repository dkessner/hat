#!/usr/bin/env python
#
# send_serial.py
#
# Darren Kessner
#
# Reads args from stdin (e.g. from web form submit POST)


import serial
import time
import sys


# parse args from stdin, return name-value map
def parseArgs():
    argMap = {}
    args = sys.stdin.readline().split('&')
    for arg in args:
        lhs, rhs = arg.split('=')
        argMap[lhs] = rhs
    return argMap


argMap = parseArgs()

ser = serial.Serial('/dev/ttyACM0', 9600)
time.sleep(3) # wait for Arduino reset

state = "nothing"

if "state" in argMap:
    state = argMap["state"]
    ser.write(state + "\n")


print("Content-type: text/html\n\n")
print("<head>")
#print('<meta http-equiv="refresh" content="3;url=http://marlpi1.local">')
print('<meta http-equiv="refresh" content="3;url=http://marlpi1">')
print("</head>")

print("<p>Sending command to Dr. Kessner's Hat</p>")

print("<ul>")
for name in argMap:
	print("<li>" + name + " " + argMap[name] + "</li>")
print("</ul>")

print("<p>Be patient...</p>")



