//
// hat_serial.ino
//
// Darren Kessner
//


#include <Adafruit_NeoPixel.h>


const int ledCount = 150;
const int columnCount = 19;
const int rowCount = 8;

const int ledPin = 6;
const int pinOnboard = 8;

Adafruit_NeoPixel leds = Adafruit_NeoPixel(ledCount, ledPin);
Adafruit_NeoPixel onboard = Adafruit_NeoPixel(1, pinOnboard);


void setup() 
{
  Serial.begin(9600);
  leds.begin();
  leds.show();
  onboard.begin();
  onboard.show();
}



//
// some useful functions
//

void show()
{
    leds.show();
}

void set(int i, uint32_t c)
{
    leds.setPixelColor(i, c);
}

void setOnboard(uint32_t value)
{
  onboard.setPixelColor(0, value);
  onboard.show();
}

void set2d(int i, int j, uint32_t c)
{
    int index = i*columnCount + j;

    if (index>0 && index<ledCount)
        leds.setPixelColor(index, c);
}

void setAll(uint32_t c)
{
    for (int i=0; i<ledCount; i++)
      leds.setPixelColor(i, c);
}

void setPixels(int iStart, int jStart, int* grid, int gridHeight, int gridWidth)
{
    for (int i=0; i<gridHeight; i++)
    for (int j=0; j<gridWidth; j++)
    {
        int ledIndex = (iStart+i)*columnCount + (jStart + j);
        if (ledIndex<0 || ledIndex>=ledCount) continue;

        int gridIndex = i*gridWidth + j;

        leds.setPixelColor(ledIndex, grid[gridIndex]);
    }
}

void unsetPixels(int iStart, int jStart, int gridWidth, int gridHeight)
{
    for (int i=0; i<gridHeight; i++)
    for (int j=0; j<gridWidth; j++)
    {
        int ledIndex = (iStart+i)*columnCount + (jStart + j);
        if (ledIndex<0 || ledIndex>=ledCount) continue;
        leds.setPixelColor(ledIndex, 0);
    }
}

// converts R,G,B to a single number
uint32_t color(uint32_t r, uint32_t g, uint32_t b)
{
    return (r&0xff)<<16 | (g&0xff)<<8 | (b&0xff);
}


// translates x in [0,1] to a rainbow color
uint32_t rainbow(float x)
{
    x -= floor(x);                  // map x to [0,1] if it's not already
    float t = 3*x - (int)(3*x);     // map each third of [0,1] to the whole interval

    if (x < 1./3)
    {
        return color((1-t)*255, t*255, 0); // R->G
    }
    else if (x < 2./3)
    {
        return color(0, (1-t)*255, t*255); // G->B
    }
    else
    {
        return color(t*255, 0, (1-t)*255); // B->R
    }
}



