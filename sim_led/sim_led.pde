//
// sim_led.pde
//
// Darren Kessner
//


enum State {Loop, Delay};
State state = State.Loop;

PGraphics pg;

int commandCount = 0;
int maxCommandCount = 0;
int commandCountActivationThreshold = 0;
int delayEndTime = 0;

int ledCount = 150;
Led[] leds = new Led[ledCount];

int rowCount = 8;
int columnCount = 19;


void setup()
{
    size(800, 400);
    pg = createGraphics(width, height);
    createLedGrid();
}


void createLedGrid()
{
    float dx = pg.width / (columnCount + 1);
    float dy = pg.height / (rowCount + 1);

    for (int index=0; index<ledCount; index++)
    {
        int i = index / columnCount;
        int j = index % columnCount;
        float x = (j+1) * dx;
        float y = (i+1) * dy;

        leds[index] = new Led(x, y);
    }
}


//
// This is the key abstraction: we need to execute the commands in loop() until
// a delay() call.  Then we need to make those commands nops so they don't
// execute the next time loop() is entered, and we can pick up where we left
// off.  
//
// Requirement: Every command called from loop() must call first call nop() and
// return immediately if returns true: 
//   if (nop()) return;
//


boolean nop() 
{
    // return true iff the command is a NOP, i.e. when we're in State.Delay
    // or haven't reached the commandCountActivationThreshold yet

    commandCount++;
    if (state == State.Delay || 
        commandCount <= commandCountActivationThreshold)
    {
        return true;
    }

    return false;
}


void show()
{
    if (nop()) return;

    pg.beginDraw();

    for (Led led : leds)
      led.display(pg);

    pg.endDraw();
}


/*
void doSomething1()
{
    if (nop()) return;

    pg.beginDraw();
    pg.background(0);
    pg.fill(0, 0, 255);
    pg.ellipse(200, 200, 50, 50);
    pg.endDraw();
}
*/


void set(int i, int c)
{
    if (nop()) return;

    if (i>=0 && i<ledCount)
      leds[i].c = c;
}


void set2d(int i, int j, int c)
{
    if (nop()) return;

    int index = i*columnCount + j;

    if (index>=0 && index<ledCount)
      leds[index].c = c;
}


void setAll(int c)
{
    if (nop()) return;

    for (int i=0; i<ledCount; i++)
      leds[i].c = c;
}


void setPixels(int iStart, int jStart, int[] grid, int gridHeight, int gridWidth)
{
    if (nop()) return;

    for (int i=0; i<gridHeight; i++)
    for (int j=0; j<gridWidth; j++)
    {
        int ledIndex = (iStart+i)*columnCount + (jStart + j);
        if (ledIndex<0 || ledIndex>=ledCount) continue;

        int gridIndex = i*gridWidth + j;

        leds[ledIndex].c = grid[gridIndex];
    }
}


void unsetPixels(int iStart, int jStart, int gridHeight, int gridWidth)
{
    if (nop()) return;

    for (int i=0; i<gridHeight; i++)
    for (int j=0; j<gridWidth; j++)
    {
        int ledIndex = (iStart+i)*columnCount + (jStart + j);
        if (ledIndex<0 || ledIndex>=ledCount) continue;
        leds[ledIndex].c = 0;
    }
}


void delay(int ms)
{
    if (nop()) return;

    commandCountActivationThreshold = commandCount; // remember which command we're on
    delayEndTime = millis() + ms;
    state = State.Delay;
}


// translates x in [0,1] to a rainbow color
int rainbow(float x)
{
    x -= floor(x);                  // map x to [0,1] if it's not already
    float t = 3*x - (int)(3*x);     // map each third of [0,1] to the whole interval

    if (x < 1./3)
    {
        int c = color((1-t)*255, t*255, 0); // R->G
        return c;
    }
    else if (x < 2./3)
    {
        return color(0, (1-t)*255, t*255); // G->B
    }
    else
    {
        return color(t*255, 0, (1-t)*255); // B->R
    }
}


void draw()
{
    background(0);
    image(pg, 0, 0, width, height);

    if (state == State.Delay)
    {
        if (millis() > delayEndTime)
            state = State.Loop;

        // if we're on the final delay() call, loop back to beginning
        if (commandCountActivationThreshold == maxCommandCount)
        {
            commandCountActivationThreshold = 0;
        }
    }
    else // state == State.Loop
    {
        commandCount = 0;   // reset commandCount before executing commands in loop()

        loop();             // execute next commands in loop()

        delay(0);           // if !nop, sets commandCountActivationThreshold = commandCount 
                            // (== maxCommandCount); this allows us to loop back to beginning
        
        if (maxCommandCount == 0)
            maxCommandCount = commandCount;
    }

    // popup text to find led index

    for (int i=0; i<rowCount; i++)
    for (int j=0; j<columnCount; j++)
    {
        int index = i*columnCount + j;
        if (index>=ledCount) break;
        Led led = leds[index];

        if (dist(led.x, led.y, mouseX, mouseY) < Led.radius)
        {
            fill(255);
            textAlign(CENTER);
            text("led [" + index + "] (" + i + "," + j + ")", led.x, led.y + 50);
        }
    }
}


class Led
{
    float x;
    float y;
    int c;

    static final float radius = 10;

    Led(float x, float y)
    {
        this.x = x;
        this.y = y;
    }

    void display(PGraphics pg)
    {
      pg.fill(c);
      pg.stroke(128);
      pg.ellipse(x, y, radius*2, radius*2);
    }
}


