//
// serial_read.ino
//


#include <Adafruit_NeoPixel.h>


const int neopixelCount = 3;
const int pinLEDs = 6;
const int pinOnboard = 8;


Adafruit_NeoPixel leds = Adafruit_NeoPixel(neopixelCount, pinLEDs);
Adafruit_NeoPixel onboard = Adafruit_NeoPixel(1, pinOnboard);



void setup() 
{
  Serial.begin(9600);
  leds.begin();
  leds.show();
  onboard.begin();
  onboard.show();
}

void loop() 
{
  if (Serial.available())
  {
     String temp = Serial.readString();
    
     if (temp.startsWith("1"))
     {
       setOnboard(color(255, 0, 0));
     }
     else if (temp.startsWith("2"))
     {
       setOnboard(color(0, 255, 0));
     }
     else
     { 
       setOnboard(0);
     }
     
     onboard.show();
  }  
}



// set the color of the onboard LED
void setOnboard(uint32_t value)
{
  onboard.setPixelColor(0, value);
  onboard.show();
}

// converts R,G,B to a single number
uint32_t color(uint32_t r, uint32_t g, uint32_t b)
{
    return (r&0xff)<<16 | (g&0xff)<<8 | (b&0xff);
}


