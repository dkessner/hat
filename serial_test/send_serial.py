#!/usr/bin/env python
#
# send_serial.py
#
# Darren Kessner
#
# Reads args from stdin (e.g. from web form submit POST)


import serial
import time
import sys


# parse args from stdin, return name-value map
def parseArgs():
    argMap = {}
    args = sys.stdin.readline().split('&')
    for arg in args:
        lhs, rhs = arg.split('=')
        argMap[lhs] = rhs
    return argMap


argMap = parseArgs()

ser = serial.Serial('/dev/ttyACM0', 9600)
time.sleep(3) # wait for Arduino reset

state = "nothing"

if "state" in argMap:
    state = argMap["state"]
    ser.write(state + "\n")


print("Content-type: text/html\n\n")
print("<h1>Hello, world!</h1>")
print("<ul>")

for name in argMap:
	print("<li>" + name + " " + argMap[name] + "</li>")

print("</ul>")


print("<p>state: " + state + "</p>")




